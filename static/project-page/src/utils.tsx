import * as React from "react";
import { JiraIssueStatusCategory } from "./types";
import { router, view } from "@forge/bridge";
import { colors } from "@atlaskit/theme";

export const GetStatusAppearance = (category: JiraIssueStatusCategory) => {
  const know = ["default", "inprogress", "moved", "new", "removed", "success"];
  let color = category.colorName;
  let type = "default";
  if (know.includes(category.key)) {
    type = category.key;
  } else {
    if (color.includes("gray")) {
      type = "default";
    } else if (color.includes("green")) {
      type = "success";
    } else if (color.includes("blue")) {
      type = "inprogress";
    } else if (color.includes("red")) {
      type = "removed";
    } else if (color.includes("yellow")) {
      type = "moved";
    }
  }

  return type;
};

export const GetStatusColor = (color: string) => {
  let type = colors.P100;

  if (color.includes("gray")) {
    type = colors.P100;
  } else if (color.includes("green")) {
    type = colors.G100;
  } else if (color.includes("blue")) {
    type = colors.B100;
  } else if (color.includes("red")) {
    type = colors.R100;
  } else if (color.includes("yellow")) {
    type = colors.Y100;
  }

  return type;
};

export const RenderKey = (key: string) => {
  return (
    <a target="_blank" onClick={() => router.open(`/browse/${key}`)}>
      {key}
    </a>
  );
};

export const GetContext = async () => {
  const context = await view.getContext();

  return Promise.resolve(context);
};

import * as React from "react";

import { Grid, GridColumn } from "@atlaskit/page";
import { JiraFullIssue } from "../types";
import { Doughnut } from "react-chartjs-2";
import { MessageModule } from "./MessageModule";
import { GetStatusColor, RenderKey } from "../utils";

interface LinkedIssueModuleProps {
  issues: Array<JiraFullIssue>;
}

export const LinkedIssueModule = ({ issues }: LinkedIssueModuleProps) => {
  const getChartData = (issues: Array<JiraFullIssue>) => {
    const data: any = {};
    const values: any = {};
    issues.forEach((issue) => {
      const category = issue.fields.status.statusCategory;

      if (!values[category.name]) {
        values[category.name] = {
          count: 0,
          color: GetStatusColor(category.colorName),
        };
      }

      values[category.name].count += 1;
    });

    data.labels = Object.keys(values);
    data.datasets = [
      {
        label: "# of issue status",
        data: Object.values(values).map((v: any) => v.count),
        backgroundColor: Object.values(values).map((v: any) => v.color),
      },
    ];

    return data;
  };

  return (
    <div>
      {issues.length > 0 ? (
        <Grid>
          <GridColumn medium={8}>
            <table>
              <thead>
                <tr>
                  <td>
                    <em>Key</em>
                  </td>
                  <td>
                    <em>Summary</em>
                  </td>
                  <td>
                    <em>Status</em>
                  </td>
                  <td>
                    <em>Priority</em>
                  </td>
                </tr>
              </thead>
              <tbody>
                {issues.map((issue) => {
                  return (
                    <tr key={issue.id}>
                      <td>{RenderKey(issue.key)}</td>
                      <td>{issue.fields.summary}</td>
                      <td>{issue.fields.status.name}</td>
                      <td>{issue.fields.priority.name}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </GridColumn>
          <GridColumn medium={4}>
            <Doughnut data={getChartData(issues)} />
          </GridColumn>
        </Grid>
      ) : (
        <Grid>
          <GridColumn>
            <MessageModule message="No linked items" appearance="information" />
          </GridColumn>
        </Grid>
      )}
    </div>
  );
};

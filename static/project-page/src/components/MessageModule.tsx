import * as React from "react";
import SectionMessage from "@atlaskit/section-message";
import { Appearance } from "@atlaskit/section-message/dist/types/types";

interface ActionType {
  text: React.ReactNode;
  call: () => void;
}

type MessageModuleProps = {
  message: string;
  actions?: Array<ActionType>;
  appearance?: Appearance;
};

const titleMap = new Map<Appearance, string>([
  ["discovery", "Discovery"],
  ["success", "Success"],
  ["error", "Error"],
  ["information", "Information"],
  ["warning", "Warning"],
]);

export const MessageModule = ({
  message,
  actions,
  appearance,
}: MessageModuleProps) => {
  let operations = [];
  if (actions) {
    operations = actions.map((action) => {
      return {
        text: action.text,
        onClick: action.call,
      };
    });
  }

  if (!appearance) appearance = "warning";

  return (
    <SectionMessage
      appearance={appearance}
      title={titleMap.get(appearance)}
      actions={operations}
    >
      <p>{message}</p>
    </SectionMessage>
  );
};

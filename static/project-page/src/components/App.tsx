import * as React from "react";
import Page, { Grid, GridColumn } from "@atlaskit/page";
import PageHeader from "@atlaskit/page-header";
import { HelpBar } from "./HelpBar";
import Spinner from "@atlaskit/spinner";
import { JiraIssue } from "../types";
import styled from "styled-components";
import { colors } from "@atlaskit/theme";
import { MessageModule } from "./MessageModule";
import Button from "@atlaskit/button";
import { IssueCardModule } from "./IssueCardModule";
import { requestJira } from "@forge/bridge";
import { GetIssues } from "../apis";
import { GetContext } from "../utils";
import { IssueDetailModule } from "./IssueDetailModule";

const Wrapper = styled.section`
  margin-top: 1em;
`;

const IssueListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 4px;
  background-color: ${colors.N20};
  margin-bottom: 16px;
  min-width: 256px;
  max-width: 256px;
  padding: 8px;
`;

const IssueCardsContainer = styled.div`
  overflow-x: auto;
  overflow-y: auto;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 75vh;
  background-color: ${colors.N0};
  padding: 4px;
`;

const IssueCardsContainerHeading = styled.div`
  display: flex;
  flex-grow: 1;
  flex-shrink: 0;
  max-height: 32px;
  padding-bottom: 8px;
`;

export const App = () => {
  const [isLoading, setLoading] = React.useState<boolean>(true);
  const [issues, setIssues] = React.useState<Array<JiraIssue>>([]);
  const [selectedIssue, selectIssue] = React.useState<JiraIssue>(null);

  React.useEffect(() => {
    const fetch = async () => {
      const context: any = await GetContext();
      const project = context.extension.project.key;
      const jiraIssues = await GetIssues(
        `project = ${project} AND "Marked As" = Contract ORDER BY created`
      );
      setIssues([...issues, ...(jiraIssues as JiraIssue[])]);
      setLoading(false);
    };
    fetch();
  }, []);

  const Issues = issues.map((issue) => (
    <IssueCardModule
      key={issue.id}
      issue={issue}
      selected={selectedIssue && selectedIssue.id}
      select={() => {
        selectIssue(issue);
      }}
    />
  ));
  const issueView = () => {
    if (issues.length > 0) {
      return (
        <Grid>
          <GridColumn medium={4}>
            <IssueListWrapper>
              <IssueCardsContainerHeading>
                <Button isDisabled={true}>Contracts</Button>
              </IssueCardsContainerHeading>
              <IssueCardsContainer>{Issues}</IssueCardsContainer>
            </IssueListWrapper>
          </GridColumn>
          <GridColumn medium={8}>
            <IssueDetailModule issue={selectedIssue} />
          </GridColumn>
        </Grid>
      );
    } else {
      return (
        <Grid>
          <GridColumn>
            <MessageModule message="No contracts found in your project" />
          </GridColumn>
        </Grid>
      );
    }
  };

  return (
    <Page>
      <Grid layout="fluid">
        <GridColumn medium={12}>
          <PageHeader actions={HelpBar}>Contract Book</PageHeader>
        </GridColumn>
      </Grid>
      {isLoading ? (
        <Grid>
          <GridColumn>
            <Spinner size="large" />
          </GridColumn>
        </Grid>
      ) : (
        <Grid layout="fluid">
          <GridColumn>
            <Wrapper>
              <Page>
                <div>{issueView()}</div>
              </Page>
            </Wrapper>
          </GridColumn>
        </Grid>
      )}
    </Page>
  );
};

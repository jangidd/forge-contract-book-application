import * as React from "react";
import Button, { ButtonGroup } from "@atlaskit/button";
import QuestionIcon from "@atlaskit/icon/glyph/question";

const HELP_URL =
  "https://bitbucket.org/jangidd/forge-contract-book-application/src/master/Documentation.md";

export const HelpBar = (
  <ButtonGroup>
    <Button
      iconBefore={<QuestionIcon label="Setting icon" size="small" />}
      href={HELP_URL}
      target="_blank"
    >
      Help
    </Button>
  </ButtonGroup>
);

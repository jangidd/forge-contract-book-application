import Page, { Grid, GridColumn } from "@atlaskit/page";
import * as React from "react";
import styled from "styled-components";
import { GetIssue, GetIssues } from "../apis";
import { JiraFullIssue, JiraIssue } from "../types";
import { MessageModule } from "./MessageModule";
import Spinner from "@atlaskit/spinner";
import { LinkedIssueModule } from "./LinkedIssueModule";
import { RenderKey } from "../utils";

interface IssueDetailModuleProps {
  issue?: JiraIssue;
}

const Wrapper = styled.div`
  margin-top: 1em;
  margin-bottom: 2px;
`;

const Well = styled.div`
  background-color: rgb(244, 245, 247);
  border-radius: 5px;
  box-sizing: border-box;
  color: rgb(52, 69, 99);
  margin-top: 20px;
  padding: 0px 8px 8px;
`;

const WellHeading = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  -webkit-box-pack: justify;
  justify-content: space-between;
  padding: 8px;
`;

const WellContent = styled.div`
  background-color: rgb(255, 255, 255);
  border-radius: 3px;
  box-shadow: rgb(0 0 0 / 10%) 0px 0px 0px 1px;
  padding: 8px;
`;

const NoIssueSelected = (
  <MessageModule message="Please select contract from side navigation bar" />
);

export const IssueDetailModule = ({ issue }: IssueDetailModuleProps) => {
  const [fullIssue, setFullIssue] = React.useState<JiraFullIssue>(null);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [cost, setCost] = React.useState<any>({});
  const [requirements, setRequirements] = React.useState<Array<JiraFullIssue>>(
    []
  );
  const [validations, setValidations] = React.useState<Array<JiraFullIssue>>(
    []
  );
  const [negotiations, setNegotiations] = React.useState<Array<JiraFullIssue>>(
    []
  );

  React.useEffect(() => {
    const fetch = async () => {
      setIsLoading(true);
      setRequirements([]);
      setValidations([]);
      setNegotiations([]);

      const full = await GetIssue(issue.id);
      let developmentCostField = null;
      let markedAsField = null;
      for (const [k, v] of Object.entries(full.names)) {
        if (v === "Development Cost") {
          developmentCostField = k;
        }
        if (v === "Marked As") {
          markedAsField = k;
        }
      }

      setCost(full.fields[developmentCostField]);

      const links = await GetIssues(
        `linkedIssue IN linkedIssues("${issue.key}")`,
        true
      );

      const requirements = [];
      const validations = [];
      const negotiations = [];

      (links as JiraFullIssue[]).forEach((i) => {
        switch (i.fields[markedAsField]) {
          case "Requirement":
            requirements.push(i);
            break;
          case "Validation":
            validations.push(i);
            break;
          case "Negotiation":
            negotiations.push(i);
            break;
        }
      });

      setNegotiations(negotiations);
      setValidations(validations);
      setRequirements(requirements);

      setFullIssue(full);
      setIsLoading(false);
    };

    if (issue) {
      fetch();
    }
  }, [issue]);

  return (
    <Page>
      {issue ? (
        <Grid>
          <GridColumn>
            <Wrapper>
              <h4>{RenderKey(issue.key)}</h4>
              <h4>{issue.fields.summary}</h4>
            </Wrapper>
            <Wrapper>
              <Well>
                <WellHeading>
                  <h5>Contract details</h5>
                </WellHeading>
                <WellContent>
                  {isLoading ? (
                    <Spinner size="medium" />
                  ) : (
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <em>Estimated Cost</em>
                          </td>
                          <td>{cost ? cost.estimated : "n.a."}</td>
                          <td>
                            <em>Original Cost</em>
                          </td>
                          <td>{cost ? cost.original : "n.a."}</td>
                        </tr>
                        <tr>
                          <td>
                            <em>Status</em>
                          </td>
                          <td>{issue.fields.status.name}</td>
                          <td>
                            <em>Priority</em>
                          </td>
                          <td>{issue.fields.priority.name}</td>
                        </tr>
                        <tr>
                          <td>
                            <em>Created Date</em>
                          </td>
                          <td>
                            {fullIssue
                              ? fullIssue.renderedFields["created"]
                              : "n.a."}
                          </td>
                          <td>
                            <em>Updated Date</em>
                          </td>
                          <td>
                            {fullIssue
                              ? fullIssue.renderedFields["updated"]
                              : "n.a."}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  )}
                </WellContent>
              </Well>
            </Wrapper>
            <Wrapper>
              <Well>
                <WellHeading>
                  <h5>Requirements</h5>
                </WellHeading>
                <WellContent>
                  {isLoading ? (
                    <Spinner size="medium" />
                  ) : (
                    <LinkedIssueModule issues={requirements} />
                  )}
                </WellContent>
              </Well>
            </Wrapper>
            <Wrapper>
              <Well>
                <WellHeading>
                  <h5>Negotiations</h5>
                </WellHeading>
                <WellContent>
                  {isLoading ? (
                    <Spinner size="medium" />
                  ) : (
                    <LinkedIssueModule issues={negotiations} />
                  )}
                </WellContent>
              </Well>
            </Wrapper>
            <Wrapper>
              <Well>
                <WellHeading>
                  <h5>Validations</h5>
                </WellHeading>
                <WellContent>
                  {isLoading ? (
                    <Spinner size="medium" />
                  ) : (
                    <LinkedIssueModule issues={validations} />
                  )}
                </WellContent>
              </Well>
            </Wrapper>
          </GridColumn>
        </Grid>
      ) : (
        <Grid>
          <GridColumn>{NoIssueSelected}</GridColumn>
        </Grid>
      )}
    </Page>
  );
};

import * as React from "react";
import styled from "styled-components";
import {
  JiraIssue,
  JiraIssuePriority,
  JiraIssueStatus,
  JiraIssueType,
} from "../types";
import Lozenge, { ThemeAppearance } from "@atlaskit/lozenge";
import { Item } from "@atlaskit/navigation-next";
import { GetStatusAppearance, RenderKey } from "../utils";
import { colors } from "@atlaskit/theme";
import CheckCircleIcon from "@atlaskit/icon/glyph/check-circle";

const ItemWrapper = styled.div`
  display: flex;
  margin-bottom: 5px;
`;

const InnerElem = styled.div`
  display: flex;
`;

const Icon = styled.span`
  display: flex;
  width: 16px;
  overflow: hidden;
  height: 16px;
`;

const TextContent = styled.span`
  margin-left: 4px;
  font-weight: 500;
  vertical-align: middle;
`;

interface IssueCardModuleProps {
  issue: JiraIssue;
  selected: string;
  select: () => void;
}

const renderType = (type: JiraIssueType) => (
  <Icon>
    <img height={16} width={16} src={type.iconUrl} title={type.name} />
  </Icon>
);

const renderPriority = (priority: JiraIssuePriority) => (
  <Icon>
    <img height={16} width={16} src={priority.iconUrl} title={priority.name} />
  </Icon>
);

const renderStatus = (status: JiraIssueStatus) => (
  <Lozenge
    maxWidth={100}
    appearance={GetStatusAppearance(status.statusCategory) as ThemeAppearance}
  >
    {status.name}
  </Lozenge>
);

export const IssueCardModule = ({
  issue,
  selected,
  select,
}: IssueCardModuleProps) => {
  const fields = issue.fields;
  return (
    <ItemWrapper>
      <Item
        onClick={() => select()}
        isSelected={issue.id === selected}
        after={() => (
          <Icon>
            {issue.id === selected ? (
              <CheckCircleIcon
                size="small"
                label="select"
                primaryColor={colors.B300}
              />
            ) : null}
          </Icon>
        )}
        before={() => (
          <span>
            {renderType(fields.issuetype)}
            {renderPriority(fields.priority)}
          </span>
        )}
        text={
          <div>
            {renderStatus(fields.status)}
            <TextContent>{RenderKey(issue.key)}</TextContent>
          </div>
        }
        subText={fields.summary}
        component={InnerElem}
      />
    </ItemWrapper>
  );
};

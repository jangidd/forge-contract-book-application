import { JiraFullIssue, JiraIssue } from "./types";
import { requestJira } from "@forge/bridge";

export const GetIssues = async (
  jql: string,
  allFields?: boolean,
  start?: number,
  maxResults?: number
): Promise<JiraIssue[] | JiraFullIssue[]> => {
  const data = {
    fields: allFields
      ? ["*all"]
      : ["summary", "issuetype", "priority", "status"],
    startAt: start || 0,
    maxResults: maxResults || 100,
    jql: jql,
  };

  const response = await requestJira("/rest/api/3/search", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  const json = await response.json();

  return allFields
    ? Promise.resolve(json.issues as Array<JiraFullIssue>)
    : Promise.resolve(json.issues as Array<JiraIssue>);
};

export const GetIssue = async (id: string): Promise<JiraFullIssue> => {
  const expand = "expand=renderedFields,names,schema";
  const response = await requestJira(
    `/rest/api/3/issue/${id}?${expand}&properties=*all`,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  const json = await response.json();

  return Promise.resolve(json);
};

export interface JiraIssuePriority {
  id: string;
  name: string;
  iconUrl: string;
  statusColor: string;
}

export interface JiraIssueStatusCategory {
  key: string;
  id: number;
  colorName: string;
  name: string;
}

export interface JiraIssueStatus {
  id: string;
  name: string;
  statusCategory: JiraIssueStatusCategory;
}

export interface JiraIssueType {
  id: string;
  name: string;
  iconUrl: string;
}

export interface JiraIssue {
  id: string;
  key: string;
  fields: {
    summary: string;
    priority: JiraIssuePriority;
    status: JiraIssueStatus;
    issuetype: JiraIssueType;
  };
}

export interface JiraFullIssue {
  id: string;
  key: string;
  fields: {
    [key: string]: any;
  };
  renderedFields: {
    [key: string]: string;
  };
  schema: {
    [key: string]: {
      type: string;
      system?: string;
      custom?: string;
      customId?: number;
    };
  };
  names: {
    [key: string]: string;
  };
}

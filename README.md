# Forge "Contract Book"

This project contains forge app written in JS, Typescript using Forge and Custom UI.

## Application documentation

Contract book is simple contract mangement application on top of Atlassian Jira

See [Documentation.md](https://bitbucket.org/jangidd/forge-contract-book-application/src/master/Documentation.md) for full documentation of this application

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials explaining Forge,
including the [documentation of Forge custom fields](https://developer.atlassian.com/platform/forge/manifest-reference/#jira-custom-field).

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

- Build UI (require Parceljs bundler)

```
cd static/project-page
yarn install
yarn build
```

- Build and deploy your app by running:

```
forge deploy
```

- Install your app in an Atlassian site by running:

```
forge install
```

- Develop your app by running `forge tunnel` to proxy invocations locally:

```
forge tunnel
```

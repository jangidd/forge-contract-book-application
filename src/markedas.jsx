import ForgeUI, {
  CustomField,
  CustomFieldEdit,
  render,
  Select,
  Option,
  Text,
  useProductContext,
  Strong,
  Em,
} from "@forge/ui";

const View = () => {
  const {
    extensionContext: { fieldValue },
  } = useProductContext();

  return (
    <CustomField>
      <Text>{fieldValue ? <Strong>{fieldValue}</Strong> : <Em>None</Em>}</Text>
    </CustomField>
  );
};

const Edit = () => {
  const onSubmit = (values) => {
    return values.selectedValue;
  };

  return (
    <CustomFieldEdit onSubmit={onSubmit} header="Issue marked as">
      <Select label="Select value" name="selectedValue">
        <Option label="Contract" value="Contract" />
        <Option label="Requirement" value="Requirement" />
        <Option label="Negotiation" value="Negotiation" />
        <Option label="Validation" value="Validation" />
      </Select>
    </CustomFieldEdit>
  );
};

export const runView = render(<View />);

export const runEdit = render(<Edit />);

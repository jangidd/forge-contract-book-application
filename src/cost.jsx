import ForgeUI, {
  CustomField,
  CustomFieldEdit,
  Em,
  Fragment,
  render,
  Strong,
  Text,
  TextField,
  useProductContext,
} from "@forge/ui";

const View = () => {
  const {
    extensionContext: { fieldValue },
  } = useProductContext();

  return (
    <CustomField>
      {fieldValue ? (
        <Fragment>
          <Text>
            Estimated Cost: <Strong>{fieldValue.estimated}</Strong>
          </Text>
          <Text>
            Original Cost: <Strong>{fieldValue.original}</Strong>
          </Text>
        </Fragment>
      ) : (
        <Text>
          <Em>none</Em>
        </Text>
      )}
    </CustomField>
  );
};

const Edit = () => {
  const onSubmit = (values) => {
    return {
      estimated: Number(values.estimated),
      original: Number(values.original),
    };
  };

  return (
    <CustomFieldEdit onSubmit={onSubmit} header="Development cost">
      <TextField
        label="Estimated Cost"
        name="estimated"
        isRequired
        type="number"
      />
      <TextField
        label="Original Cost"
        name="original"
        isRequired
        type="number"
      />
    </CustomFieldEdit>
  );
};

export const runView = render(<View />);

export const runEdit = render(<Edit />);

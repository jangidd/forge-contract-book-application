# Contract Book

A simple application to manage your bussiness contracts in Atlassian Jira

## Configurations

This application introduces two custom fields in your Jira.

- Markes As
  
      Purpose of this custom field is to categorise issues in to Contract, Requirement, Negotiation, and Validation
  
- Development Cost
  
      Purponse of this field is to add estimated and original cost to contracts, requirements and negotiations.

To use this application please add these fields to your Jira Project Field Configuration and appropriate screens. As documented here [https://support.atlassian.com/jira-cloud-administration/docs/manage-issue-field-configurations/](https://support.atlassian.com/jira-cloud-administration/docs/manage-issue-field-configurations/)

## Ethymology

![Diagram](screenshots/diagram.png)

Application classify issues in to four types (using `Marked As` custom field),
- Contract:
  
      A formal document for any business need

- Requirement
  
      A break down of contract in atomic sections

- Negotiation
  
      A negotiation done on contracts

- Validation
  
      Validation criteria for fulfillment of contract

Here, for this application `Contract` is major entity, once you classify any issue as `Contract` it will start appering in `Contract Book` project page.

Once `Contract` is marked you can link `Requirement`, `Negotation` and `Validation` using Jira's build in issue-link functionality.


For simplicity we have not tweaked any other flow of Jira, you can use Jira's workflow to manage various stage of your contract.

## Screenshots

- Main Screen
![Main Screen](screenshots/1-main.png)

- Mark As Custom field
![Mark as field](screenshots/2-marking.png)

- Development cost Custom field
![Development cost field](screenshots/3-cost.png)
